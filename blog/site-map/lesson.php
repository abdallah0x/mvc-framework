<?php

#=======================================================================================================#
#                                      	    1 - htaccess				              	             	#  
#=======================================================================================================#

/* create index page and generate clean url
/* xampp
/* php version 5.4 + 
/* Apache mod_rewrite enable => check phpinfo()
/* ide


#=======================================================================================================#
#                                 2 - Application Class + File System			           	           	#  
#=======================================================================================================#
    /** Prerequisites */
/* Basic oop
/* Magic Constants [ like __DIR__ , ... ]
/* Type Hinting
/* Magic Method
/* Autoload Classes
/* Access Object As Array [ArrayAccess Interface]

/* File System
/* Autoloading Classes
/* Sharing Data | Objects

/*=======================================================================================================#
#                                      	    Session Class				              	             	#  
#=======================================================================================================#

/* Session::start 	==> start session
/* Session::set 	==> set add new value to session
/* Session::get 	==> get value from session
/* Session::has 	==> Determine if session has the given key 
/* Session::remove 	==> remove the given key from session 
/* Session::all 	==> get all session data
/* Session::pull 	==> get the value of the given key and remove it
/* Session::destroy ==> destroy session 



/*=======================================================================================================#
#                                      	    Request Class				              	             	#  
#=======================================================================================================#

/* Request::prepareUrl 	==> Prepare url and set it
/* Request::get 		==> Get value from _GET
/* Request::post 		==> Get value from _POST
/* Request::server 		==> Get value from _SERVER 
/* Request::method 		==> Get request method
/* Request::baseUrl 	==> Get Domain path
/* Request::url 		==> Get request url 


/*=======================================================================================================#
#                                      	     Route Class			   	              	               	 #  
#========================================================================================================#
/* $url    = /blog/users/add (for example)
/* $action = controller@method
/* void   Route::add($url,$action,$requestMethod = GET) ==> Add new Route
/* string Route::generatePattern($url) 		            ==> Generate regex pattern for the given url 
/* string Route::getAction($action) 	              	==> Get Action which contains controller
/* void   Route::notFound($url) 	                   	==> Set not found url that will be redirect if
/* array  Route::getProperRoute 	                  	==> Get the controller , method and its passed argument
/* bool   Route::isMatching($pattern)                  	==> Determine if the given pattern matches the current request url
/* array  Route::getArgumentsFrom($pattern)  	        ==> Get Route url 


/*=======================================================================================================#
#                                      	    Cookie Class				              	             	#  
#=======================================================================================================#

/* Cookie::start 	==> start session
/* Cookie::set 	==> set add new value to session
/* Cookie::get 	==> get value from session
/* Cookie::has 	==> Determine if session has the given key 
/* Cookie::remove 	==> remove the given key from session 
/* Cookie::all 	==> get all session data
/* Cookie::pull 	==> get the value of the given key and remove it
/* Cookie::destroy ==> destroy session 

