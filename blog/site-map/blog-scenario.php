<?php 

/*#######################################################################################################*/
 #											In Frontend													#
/*#######################################################################################################/


#=======================================================================================================#
#                                      		   Pages					              	             	#  
#=======================================================================================================#

/* Home Page
/* Post Page
/* Login Page
/* Category Page
/* Contact Us Page
/* Registration Page

#=======================================================================================================#
#                                          Layout System					              	          	#  
#=======================================================================================================#

/*Layout System is a Layout for all pages which will contain :
	/* Header
	/* Page Content
	/* Footer
/* So the content of any page from the above ones will be set in that section

#=======================================================================================================#
#                                        Home Page Scenario 		                                    #  
#=======================================================================================================#

/* In this page we will display our latest posts
/* Displaying them vertically so each post
/* A sidebar that will contain recommended posts , some ads etc..

#=======================================================================================================#
#                                        Post Page Scenario 		                                 	#  
#=======================================================================================================#

/* This will contain the post its self
/* Related posts for the current post
/* Comment for the post
/* Some ads in sidebar

#=======================================================================================================#
#                                        Login Page Scenario 		                               		#  
#=======================================================================================================#

/* We will display a login form consisting of the following inputs : 
	/* Email
	/* password
	/* Remember Me as Checkbox

#=======================================================================================================#
#                                       Category Page Scenario 		                                 	#  
#=======================================================================================================#

/* This page will contain all posts related to that category 
/* It will also woll contain a sidebar to display its sub categories 
/* Some keywords from its posts (Tags)
/* Some Ads

#=======================================================================================================#
#                                       Contact Us Page Scenario 		                          		#  
#=======================================================================================================#

/* Contact form to contact the blog administration
/* Which may contain the following inputs :
	/* Name
	/* Email
	/* Phone
	/* Subject
	/* Message
	/* ReCaptcha "for spams"

#=======================================================================================================#
#                                       Registration Page Scenario 		                                #  
#=======================================================================================================#

/* The Registration form will contain the following :
	/* First name
	/* Last name
	/* Email
	/* Password
	/* Profile Image
	/* Gender
	/* Birth Day
	/* ReCaptcha " for spams"


/*#######################################################################################################*/
 #											    In Backend												#
/*#######################################################################################################/

#=======================================================================================================#
#                                      		       Pages					              	            #  
#=======================================================================================================#

/* Dashboard Page
/* Ads Page
/* Login Page
/* Settings Page
/* Users Page CRUD (Displaying users + Create + Update + Delete )
/* Users Group CRUD " for permissions "
/* Categories Page CRUD
/* Posts page CRUD
/* Contact Us page ( to via + replay + delete)

#=======================================================================================================#
#                                      		   Layout System					              	       	#  
#=======================================================================================================#

/*Layout System is a Layout for all pages which will contain :
	/* Header
	/* Sidebar
	/* Page Content
	/* Footer
/* So the content of any page from the above ones will be set in that section

#=======================================================================================================#
#                                         Dashboard Page Scenario		              	             	#  
#=======================================================================================================#

/* In this page we will display all reports about everything
/* Online users
/* Total registration users " all + today "
/* Total posts " all + today "
/* Mostly viewed posts " all + today "
/* etc


#=======================================================================================================#
#                                           Login Page Scenario 		               					#  
#=======================================================================================================#

/* We will display a login form consisting of the following inputs : 
	/* Email
	/* password
	/* Remember Me as Checkbox

#=======================================================================================================#
#                                           Other Pages Scenario 		                               	#  
#=======================================================================================================#

/* When opening "users || users group || posts || categories page"
/* we will display all data about this page
/* In table with some controls " editing - deleting "
/* Also a button for creating new user , post , category ... etc


#=======================================================================================================#
#                                         Contact Us Page Scenario                        				#  
#=======================================================================================================#

/* Displaying all contacts in table
/* With some controls " view the message + replying + deleting"

#=======================================================================================================#
#                                          Settings Page Scenario 		                               	#  
#=======================================================================================================#

/* This page will contain all site settings

#=======================================================================================================#
#                                            Ads Page Scenario 		                          			#  
#=======================================================================================================#

/* This page will ads setting


