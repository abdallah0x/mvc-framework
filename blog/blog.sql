-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2015 at 08:08 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'News'),
(3, 'Event');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `auther` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category`, `title`, `body`, `auther`, `tags`, `date`) VALUES
(1, 1, 'International PHP Conference 2014', '			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pretium commodo felis vel ultrices. Etiam id dui eros. Praesent nunc dui, volutpat at eleifend nec, suscipit a sapien. Morbi leo urna, aliquam a purus eget, tempus cursus nisl. In hac habitasse platea dictumst. Aliquam pulvinar at lorem vel vulputate. Nunc tempus enim neque. Fusce justo nibh, volutpat eget auctor et, malesuada quis odio. Suspendisse convallis consequat lectus, ullamcorper consequat mauris luctus in. Suspendisse accumsan lorem varius tincidunt tristique. Ut nulla libero, feugiat vitae dui ac, dictum adipiscing libero. Sed sit amet augue mi. Nullam luctus ligula ultricies erat volutpat scelerisque. Etiam molestie sodales aliquam. Donec nisl odio, fringilla quis nibh quis, pulvinar ornare nibh. Integer lorem ligula, scelerisque ut felis et, pretium ultricies arcu.</p>\r\n           ', 'Abdallah alfeky', 'php ', '2015-05-17 08:16:30'),
(2, 3, 'PHP 5.6.0beta4 Released', ' <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pretium commodo felis vel ultrices. Etiam id dui eros. Praesent nunc dui, volutpat at eleifend nec, suscipit a sapien. Morbi leo urna, aliquam a purus eget, tempus cursus nisl. In hac habitasse platea dictumst. Aliquam pulvinar at lorem vel vulputate. Nunc tempus enim neque. Fusce justo nibh, volutpat eget auctor et, malesuada quis odio. Suspendisse convallis consequat lectus, ullamcorper consequat mauris luctus in. Suspendisse accumsan lorem varius tincidunt tristique. Ut nulla libero, feugiat vitae dui ac, dictum adipiscing libero. Sed sit amet augue mi. Nullam luctus ligula ultricies erat volutpat scelerisque. Etiam molestie sodales aliquam. Donec nisl odio, fringilla quis nibh quis, pulvinar ornare nibh. Integer lorem ligula, scelerisque ut felis et, pretium ultricies arcu.</p>\r\n          ', 'Abdallah alfeky', 'php', '2015-05-17 08:16:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
