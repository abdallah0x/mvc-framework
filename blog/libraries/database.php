<?php

/**
 * Description of database
 *
 * @author lef
 */
class database 
{
    public $host     = DB_HOST;
    public $user     = DB_USER;
    public $pass     = DB_PASS;
    public $database = DB_NAME;
    
    public $link;
    public $error;
    
    
    public function __construct()
    {
        $this->connect();
    }
    
    
    function connect()
    {
        $this->link = new mysqli($this->host, $this->user, $this->pass, $this->database);
        
        if(!$this->link)
        {
            $this->error = 'connection error' . $this->link->connect_error;
            return FALSE;
        }
    }
    
    
    function select($query)
    {
        $result = $this->link->query($query) or die($this->link->error . __LINE__);
        
        if($result->num_rows > 0)
        {
            return $result;
        }
        return FALSE;
    }
    
    
    function insert($query)
    {
        $insert_row = $this->link->query($query) or die($this->link->error . __LINE__);
        
        if($insert_row)
        {
            header("Location: index.php?msg=".urlencode('inserted done'));
           // exit();
        }  else {
            die('Error : ('.$this->link->errno.') '.$this->link->error);
        }
    }
    
    
     
    function delete($query)
    {
        $delete_row = $this->link->query($query) or die($this->link->error . __LINE__);
        
        if($delete_row)
        {
            header("Location:index.php?msg=".urlencode('deleted done'));
            exit();
        }  else {
            die('Error : ('.$this->link->errno.') '.$this->link->error);
        }
    }
    
    
     
    function update($query)
    {
        $update_row = $this->link->query($query) or die($this->link->error . __LINE__);
        
        if($update_row)
        {
            header("Location:index.php?msg=".urlencode('updated done'));
            exit();
        }  else {
            die('Error : ('.$this->link->errno.') '.$this->link->error);
        }
    }
 

 

 
}
