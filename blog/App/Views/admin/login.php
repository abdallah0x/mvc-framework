<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Login</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo assets('admin/css/bootstrap.min.css')?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo assets('admin/css/font-awesome.min.css')?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo assets('admin/css/ionicons.min.css')?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo assets('admin/css/AdminLTE.min.css')?>">

        <link rel="stylesheet" href="<?php echo assets('admin/plugins/iCheck/square/blue.css')?>">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>BLOG</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <div style="font-weight:bold" id="login-result"></div>
            <form id="login-form" action="<?php echo url('/admin/login/submit')?>" method="post">
                        <div class="form-group has-feedback">
                            <input type="text" name='email' id="email" class="form-control" placeholder="Email" value="">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" id="password" name='password' class="form-control" placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                <input type="checkbox" name="remember" > Remember Me
                                </label>
                            </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

            <!-- <a href="#">I forgot my password</a><br>
            <a href="{{url('/')}}" class="text-center">Register a new membership</a> -->

        </div>
        <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="<?php echo assets('admin/js/jquery.min.js')?>"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo assets('admin/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo assets('admin/plugins/iCheck/icheck.min.js')?>"></script>
        <script>
            $(document).ready(function() {
                $(function () {
                    $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                    });
                });


                loginResult = $('#login-result')
                $("#login-form").submit(function(e){
                    e.preventDefault();
                    data = $(this).serialize();

                    $.ajax({
                        type : $(this).attr('method'),
                        url  : $(this).attr('action'),
                        data : data,
                        dataType : 'json',

                        beforeSend : function(){
                            loginResult.removeClass().addClass('alert alert-info').html('logging...');
                            $('button').append(' <i class="fa fa-spinner fa-spin"></i> ')
                                .attr('disabled','disabled');
                        },

                        success : function(results){
                            if(results.errors){
                                loginResult.removeClass().addClass('alert alert-danger').html(results.errors);
                                $('.fa-spinner').remove();
                                $('button').removeAttr('disabled');
                            }else if(results.success){
                                $('#email').val('');
                                $('#password').val('');
                                loginResult.removeClass().addClass('alert alert-success').html(results.success);
                                window.location.href = results.redirect;
                                
                                // setTimeout(() => {
                                //     window.location.href = results.redirect;
                                // }, 2000);
                            }                            
                        }
                    });
                    
                });
            });
        </script>
    </body>
</html>