<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">Test</a>.</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo assets('admin/js/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo assets('admin/js/bootstrap.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo assets('admin/js/adminlte.min.js')?>"></script>

<script src="<?php echo assets('admin/sweetalert.min.js')?>"></script>

<script>

    $(function(){
        currentUrl = window.location.href;
        
        segment = currentUrl.split('/').pop();

        $('#' + segment + '-link').addClass('active');    
    })
        
    $(document).ready(function() {

        



        $('.open-pupup').click(function(){
            btn = $(this);
            url = btn.data('target')
            modal = btn.data('modal')
            
            if($(modal).length > 0){
                $(modal).modal('show')
            }else{
                $.ajax({
                    url : url,
                    type: 'GET',
                    success : function(html){
                        $('body').append(html)
                        $(modal).modal('show')
                    }
                })
            }
            
        })

        $(document).on('click', '.submit-btn', function(e){
            e.preventDefault()
            btn = $(this);
            form = $('.form-model')

            url = form.attr('action')
            data = new FormData(form[0]);

            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : url,
                data : data,
                cache : false,
                processData : false,
                contentType : false, 

                beforesend : function(){
                    $('#form-results').removeClass().addClass('alert alert-info').html('Loading...');
                },

                success : function(results){
                    if(results.errors){
                        $('#form-results').removeClass().addClass('alert alert-danger').html(results.errors);
                    }else if(results.success){
                        $('#form-results').removeClass().addClass('alert alert-success').html(results.success);
                    }

                    if(results.redirectTo){
                        window.location.href = results.redirectTo;
                    }
                },

            })
        })


        // $('form').on('submit', function(e)){
        //     e.preventDefault();

        //     var form = $(this);

        //     var data = new FormData(form[0]);

        //     $.ajax({
        //         url : form.attr('action'),
        //         type : 'post',
        //         data : data,
        //         dataType : 'json',
        //         cache : false,
        //         processData : false,
        //         contentType : false, 

        //         success : function(r){

        //         }
        //     })
        // }


        /* Deleting */
        $('.delete').click(function(e){
            e.preventDefault();

            button = $(this)
            url = button.data('target');
            swal({
                    title: "Are You Sure ?",
                    type: "error",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    showCancelButton: true,
                },
                function() {
                    $.ajax({
                        type: "GET",
                        url: url,

                        beforeSend: function(){
                            $('#result').removeClass().addClass('alert alert-info').html('Deleting...')
                        },

                        success: function () {
                            $('#result').removeClass().addClass('alert alert-success').html('Category Deleted Successfully')
                            tr = button.parents('tr')
                            tr.fadeOut(700, function() { tr.remove(); });
                        },
                        error: function(){
                            swal("Danger", "Error ...", "error");
                        }       
                    });
            });
        })
    });
</script>

</body>
</html>