

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="<?php echo assets('admin/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>name</p>
      <!-- Status -->
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">


    <li id="categories-link">
        <a href="<?php echo url('/admin/categories') ?>">
        <i class="fa fa-car"></i> <span>Categories</span>
        </a>
    </li>

    <li id="users-groups-link"> 
        <a href="<?php echo url('/admin/users-groups') ?>">
        <i class="fa fa-car"></i> <span>Users Groups</span>
        </a>
    </li>

    <li id="users-link">
        <a href="<?php echo url('/admin/users') ?>">
        <i class="fa fa-car"></i> <span>Users</span>
        </a>
    </li>

  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>