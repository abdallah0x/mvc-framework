  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
        Simple Tables
        <small>preview of simple tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Category : <?php echo $category->name ?></h3>
              <button class="btn btn-primary pull-right open-pupup" data-modal="#add-category" data-target="<?php echo url('/admin/categories/add') ?>" type="button">Add New Category</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php if ($errors) {?>
              <div class="alert alert-danger"><?php echo implode('<br>', $errors) ?></div>
            <?php } ?>
            <div id="form-results"></div>
            <form action="<?php echo $action ?>" method="POST">
                <div class="form-group col-sm-6">
                    <label for="name">Category Name</label>
                    <input type="text" placeholder="Name" id="name" value="<?php echo $category->name ?>" name="name" class="form-control">
                </div>

                <div class="form-group col-sm-6">
                    <label for="status">Status</label>
                    <select name ="status" id="status" class="form-control">
                          <option value="enabled">Enabeld</option>
                          <option value="disabled" <?php echo $category->status == 'disabled' ? 'selected' : '' ?> >Disabled</option>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                  <button class="btn btn-info">Submit</button>
                </div>
            </form>
            </div>
            
        </div>
        <!-- /.box -->
    </div>
</section>

  </div>
  <!-- /.content-wrapper -->