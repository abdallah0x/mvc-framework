  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
        Simple Tables
        <small>preview of simple tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Manager Your users</h3>
              <button class="btn btn-primary pull-right open-pupup" data-modal="#add-user-form" data-target="<?php echo url('/admin/users/add') ?>" type="button">Add New user</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="result"></div>
              <table class="table table-bordered">
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Group Name</th>
                  <th>E-mail</th>
                  <th>Status</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
                <?php foreach($users as $user) { ?>
                  <tr>
                    <td><?php echo $user->id ?></td>
                    <td>
                      <img src="<?php echo assets('images/' . $user->image) ?>" style="border-radius:50%" width="40" hieght="50">
                      <?php echo $user->first_name . ' ' . $user->last_name ?>
                    </td>
                    <td><?php echo ucfirst($user->group) ?></td>
                    <td><?php echo $user->email ?></td>
                    <td><?php echo ucfirst($user->status) ?></td>
                    <td><?php echo date('Y-m-d',$user->created_at) ?></td>
                    <td>
                      <button type="button" href='<?php echo url("admin/users/edit/$user->id"); ?>'
                              class="btn btn-primary open-pupup"
                              data-modal="#edit-user-<?php echo $user->id ?>"
                              data-target='<?php echo url("admin/users/edit/$user->id"); ?>'>
                        Edit
                        <span class="fa fa-pencil"></span>
                      </button>
                      <?php if($user->id != 1) {?>
                      <button data-target='<?php echo url("admin/users/delete/$user->id"); ?>' class="btn btn-danger delete">
                        Delete
                        <span class="fa fa-trash"></span>
                      </button>
                      <?php } ?>
                    </td>
                  </tr>
                <?php } ?>
              </table>
            </div>
            
        </div>
        <!-- /.box -->
    </div>
</section>

  </div>
  <!-- /.content-wrapper -->