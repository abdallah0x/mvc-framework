<div class="modal fade" id="<?php echo $target?>">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $heading ?></h4>
        </div>
        <div class="modal-body">
            <div id="form-results"></div>
            <form action="<?php echo $action ?>" class="form-model">
                <div class="form-group col-sm-6">
                    <label for="first_name">First Name</label>
                    <input type="text" placeholder="First Name" id="first_name" value="<?php echo $first_name ?>" name="first_name" class="form-control">
                </div>

                <div class="form-group col-sm-6">
                    <label for="last_name">Last Name</label>
                    <input type="text" placeholder="Last Name" id="last_name" value="<?php echo $last_name ?>" name="last_name" class="form-control">
                </div>


                <div class="form-group col-sm-6">
                    <label for="users_group_id">Users Group</label>
                    <select name ="users_group_id" id="users_group_id" class="form-control">
                        <?php foreach($groups as $group) { ?>
                            <option value="<?php echo $group->id ?>" <?php echo $group->id == $users_group_id ? 'selected' : '' ?>><?php echo $group->name ?></option>
                        <?php } ?>
                    </select>
                </div>


                <div class="form-group col-sm-6">
                    <label for="email">E-mail</label>
                    <input type="text" placeholder="E-mail" id="email" value="<?php echo $email ?>" name="email" class="form-control">
                </div>

                <div class="form-group col-sm-6">
                    <label for="password">Password</label>
                    <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                </div>


                <div class="form-group col-sm-6">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                </div>

                <div class="form-group col-sm-6">
                    <label for="birthday">Birthday</label>
                    <input type="text" placeholder="Birthday" id="birthday" value="<?php echo $birthday ?>" name="birthday" class="form-control">
                </div>

                

                <div class="form-group col-sm-6">
                    <label for="image">image</label>
                    <input type="file" id="image" name="image" class="form-control">
                </div>

                

                <div class="form-group col-sm-6">
                    <label for="gender">Gender</label>
                    <select name ="gender" id="gender" class="form-control">
                        <option value="male">Male</option>
                        <option value="female" <?php echo $gender == 'female' ? 'selected' : '' ?>>Female</option>
                    </select>
                </div>
                <?php if($image) { ?> 
                    <div class="form-group col-sm-6">
                        <img src="<?php echo $image ?>" width="100" hieght="100" >
                    </div>
                <?php } ?>

                <div class="form-group col-sm-6">
                    <label for="status">Status</label>
                    <select name ="status" id="status" class="form-control">
                        <option value="enabled">Enabeld</option>
                        <option value="disabled" <?php echo $status == 'disabled' ? 'selected' : '' ?>>Disabled</option>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <button type="button" class="btn btn-info submit-btn" id="submit-btn">Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->