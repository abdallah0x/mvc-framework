  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
        Simple Tables
        <small>preview of simple tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Manager Your Users Group</h3>
              <button class="btn btn-primary pull-right open-pupup" data-modal="#add-users-group-form" data-target="<?php echo url('/admin/users-groups/add') ?>" type="button">Add New Users Group</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="result"></div>
              <table class="table table-bordered">
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>
                </tr>
                <?php foreach($usersGroups as $usersGroup) { ?>
                  <tr>
                    <td><?php echo $usersGroup->id ?></td>
                    <td><?php echo $usersGroup->name ?></td>
                    <!-- <td><?php echo ucfirst($usersGroup->status) ?></td> -->
                    <td>
                      <button type="button" href='<?php echo url("admin/users-groups/edit/$usersGroup->id"); ?>'
                              class="btn btn-primary open-pupup"
                              data-modal="#edit-users-group-<?php echo $usersGroup->id ?>"
                              data-target='<?php echo url("admin/users-groups/edit/$usersGroup->id"); ?>'>
                        Edit
                        <span class="fa fa-pencil"></span>
                      </button>
                      <?php if($usersGroup->id != 1) { ?>
                        <button data-target='<?php echo url("admin/users-groups/delete/$usersGroup->id"); ?>' class="btn btn-danger delete">
                          Delete
                          <span class="fa fa-trash"></span>
                        </button>
                      <?php } ?>
                    </td>
                  </tr>
                <?php } ?>
              </table>
            </div>
            
        </div>
        <!-- /.box -->
    </div>
</section>

  </div>
  <!-- /.content-wrapper -->