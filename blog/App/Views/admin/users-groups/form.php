<div class="modal fade" id="<?php echo $target?>">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $heading ?></h4>
        </div>
        <div class="modal-body">
            <div id="form-results"></div>
            <form action="<?php echo $action ?>" class="form-model">
                <div class="form-group col-sm-12">
                    <label for="name">Users Group Name</label>
                    <input type="text" placeholder="Name" id="name" value="<?php echo $name ?>" name="name" class="form-control">
                </div>

                <div class="form-group col-sm-12">
                    <label for="pages">Pages</label>
                    <select name="pages[]" id="pages" multiple class="form-control">
                        <?php foreach($pages as $page) { ?>
                            <option value="<?php echo $page ?>" <?php echo in_array($page, $userGroupPages) ? 'selected' : '' ?> ><?php echo $page ?></option>
                        <?php } ?>
                    </select>
                </div>

                <!-- <div class="form-group col-sm-6">
                    <label for="status">Status</label>
                    <select name ="status" id="status" class="form-control">
                        <option value="enabled">Enabeld</option>
                        <option value="disabled" <?php echo $status == 'disabled' ? 'selected' : '' ?>>Disabled</option>
                    </select>
                </div> -->
                <div class="form-group col-sm-6">
                    <button type="button" class="btn btn-info submit-btn" id="submit-btn">Submit</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->