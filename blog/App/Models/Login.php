<?php 

namespace App\Models;

use System\Model;

class Login extends Model
{
    /**
     * Table name
     * 
     * @var string
     */
    protected $table = 'users';


    /**
     * Logged In User
     * 
     * @var \stdClass
     */
    protected $user;


    /**
     * Determine if the given login data is valid
     * 
     * @param string $email
     * @param string $password
     * 
     * @return bool
     */
    public function isValid($email, $password)
    {
        $user = $this->where('email=?', $email)->fetch($this->table);
        
        if(! $user){
            return false;
        }

        if(password_verify($password, $user->password)){
            $this->user = $user;
            return true;
        }else{
            return false;
        }
    }


    /**
     * Get Logged In User data
     * 
     * @return stdClass
     */
    public function user()
    {
        return $this->user;
    }


    /**
     * Determine if User logged in or not
     * 
     * @return bool
     */
    public function isLogged()
    {
        if($this->cookie->has('login')){
            $code = $this->cookie->get('login');
        }elseif($this->session->has('login')){
            $code = $this->session->get('login');
        }else{
            $code = '';
        }

        $user = $this->where('code = ?', $code)->fetch($this->table);

        if(! $user){
            return false;
        }
        $this->user = $user;
        
        return true;
    }
}