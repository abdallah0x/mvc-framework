<?php 


namespace App\Controllers\Admin;

use System\Controller;

class DashboardController extends Controller
{
    /**
     * Display Login Form
     * 
     * @return mixed
     */
    public function index()
    {
        return $this->view->render('/admin/dashboard');
    }
      
}