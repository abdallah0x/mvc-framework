<?php 


namespace App\Controllers\Admin;

use System\Controller;

class AccessController extends Controller
{
    /**
     * Check user Permissions to access admin pages
     * 
     * @return mixed
     */
    public function index()
    {
        
        $loginModel = $this->load->model('Login');

        $ignoredPages = ['/admin/login', '/admin/login/submit'];

        $currentPage = $this->request->url();

        if(! $loginModel->isLogged() && in_array($currentPage, $ignoredPages)){
            //return $this->url->redirectTo('/admin/login');
            return $this->view->render('admin/login');
        }

        // if($loginModel->isLogged()){
        //     return false;
        // }
        $user = $loginModel->user();

        $usersGroupsModel = $this->load->model('UsersGroup');
        
        $usersGroup = $usersGroupsModel->get($user->users_group_id);
        // if(! in_array($currentPage, $usersGroup->pages)){
        //     return $this->url->redirectTo('/');
        // }
    }

}