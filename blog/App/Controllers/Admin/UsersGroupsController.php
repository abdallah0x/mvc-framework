<?php 


namespace App\Controllers\Admin;

use System\Controller;

class UsersGroupsController extends Controller
{
    /**
     * Display Users Groups List
     * 
     * @return mixed
     */
    public function index()
    {
        $this->html->setTitle('Users Groups');

        $data['usersGroups'] = $this->load->model('UsersGroup')->all();        
        
        $data['success'] = $this->session->has('success') ? $this->session->pull('success') : null;
        
        $view = $this->view->render('admin/users-groups/list', $data);

        return $this->adminLayout->render($view);
    }


    /**
     * Open Users Groups Form
     * 
     * @return string
     */
    public function add()
    {
        return $this->form();
    }


    /**
     * Submit for creating new User Group
     * 
     * @return string | json
     */
    public function submit()
    {
        $json = [];

        if($this->isValid()){

            $this->load->model('UsersGroup')->create();

            $json['success'] = 'User Group Has Been Creared Successfully';

            $json['redirectTo'] = $this->url->link('admin/users-groups');
        }else{
            
            $json['errors'] = $this->validator->flattenMessages();

        }

        return $this->json($json);
    }


    /**
     * Display Edit Form
     * 
     * @param int $id
     * @return string
     */
    public function edit ($id) 
    {
        $usersGroup = $this->load->model('UsersGroup');

        if(! $usersGroup->exists($id)){
            return $this->url->redirectTo('/404');
        }

        $usersGroup = $usersGroup->get($id);

        return $this->form($usersGroup);

    }


    /**
     * Display Form
     * 
     * @param \stdClass $userGroup
     * @return string
     */
    public function form($usersGroup = null)
    {
        if($usersGroup){
            // edit users-group
            $data['target'] = 'edit-users-group-' . $usersGroup->id;

            $data['heading'] = 'Edit ' . $usersGroup->name;

            $data['action'] = $this->url->link("/admin/users-groups/save/$usersGroup->id");

        }else{
            // add users-group
            $data['target'] = 'add-users-group-form';

            $data['heading'] = 'Add New Users Group';

            $data['action'] = $this->url->link('/admin/users-groups/submit');
            
        }

        $data['name'] = $usersGroup ? $usersGroup->name : null;

        $data['userGroupPages'] = $usersGroup ? $usersGroup->pages : [];

        // $data['status'] = $usersGroup ? $usersGroup->status : 'enabled';

        $data['pages'] = $this->getPermissionPages();

        return $this->view->render('admin/users-groups/form', $data);
    }


    /**
     * Get All Permission Pages
     * 
     * @return array
     */
    private function getPermissionPages()
    {
        $permissions = [];

        foreach($this->route->routes() as $route){
            if(strpos($route['url'], '/admin') === 0){
                $permissions[] = $route['url'];
            }
        }

        return $permissions;
    }


    /**
     * Submit for creating new User Group
     * 
     * @return string | json
     */
    public function save($id)
    {
        $json = [];
        
        if($this->isValid()){

            $this->load->model('UsersGroup')->update($id);

            $json['success'] = 'User Group Has Been Updated Successfully';

            $json['redirectTo'] = $this->url->link('admin/users-groups');
        }else{

            $json['errors'] = $this->validator->flattenMessages();
        }

        return $this->json($json);
    }


    /**
     * Validate the form
     * 
     * @return bool
     */
    private function isValid()
    {
        $this->validator->required('name', 'User Group Name Is Required');

        return $this->validator->passes();
    }


    /**
     * Delete User Group
     * 
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $usersGroup = $this->load->model('UsersGroup');

        if(! $usersGroup->exists($id) || $id == 1){
            return $this->url->redirectTo('/404');
        }

        $usersGroup->delete($id);
    }
      
}