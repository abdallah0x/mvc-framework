<?php 


namespace App\Controllers\Admin;

use System\Controller;

class LoginController extends Controller
{
    /**
     * Display Login Form
     * 
     * @return mixed
     */
    public function index()
    {
        $loginModel = $this->load->model('Login');
        if($loginModel->isLogged()){
            return $this->url->redirectTo('/admin');
        }
        return $this->view->render('admin/login', ['errors' => $this->errors]);
    }


    /**
     * Submit Login Form
     * 
     * @return mixed
     */
    public function submit()
    {
        if($this->isValid()){
            $loginModel = $this->load->model('Login')->user();
            
            if($this->request->post('remember')){
                // save login data in cookie
                $this->cookie->set('login', $loginModel->code);
            }else{
                // save login data in session
                $this->session->set('login', $loginModel->code);
            }

            //return $this->url->redirectTo('/admin');
            $json = [];
            $json['success']  = 'Welcome Back ' . $loginModel->first_name;
            $json['redirect'] = $this->url->link('/admin');

            return $this->json($json);
        }else{
            //return $this->index();
            $json = [];
            $json['errors']  = implode('<br>', $this->errors);

            return $this->json($json);
        }

    }


    /**
     * Validate Login Form
     * 
     * @return bool
     */
    private function isValid()
    {
        $email    = $this->request->post('email');
        $password = $this->request->post('password');

        if(! $email){
            $this->errors[] = 'Please Insert Email Address';
        }elseif(! filter_var($email, FILTER_VALIDATE_EMAIL)){
            $this->errors[] = 'Please Insert Valid Email';
        }

        if(! $password){
            $this->errors[] = 'Please Insert Password';
        }

        if(! $this->errors){
            $loginModel = $this->load->model('Login');
            if(! $loginModel->isValid($email, $password)){
                $this->errors[] = 'Invalid Login Data';
            }
        }

        return empty($this->errors); 

    }

      
}