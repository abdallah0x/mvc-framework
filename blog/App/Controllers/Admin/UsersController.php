<?php 


namespace App\Controllers\Admin;

use System\Controller;

class UsersController extends Controller
{
    /**
     * Display Users List
     * 
     * @return mixed
     */
    public function index()
    {
        $this->html->setTitle('Users');

        $data['users'] = $this->load->model('User')->all();        

        $data['success'] = $this->session->has('success') ? $this->session->pull('success') : null;
        
        $view = $this->view->render('admin/users/list', $data);

        return $this->adminLayout->render($view);
    }


    /**
     * Open Users Form
     * 
     * @return string
     */
    public function add()
    {
        return $this->form();
    }


    /**
     * Submit for creating new User Group
     * 
     * @return string | json
     */
    public function submit()
    {
        $json = [];

        if($this->isValid()){

            $this->load->model('User')->create();

            $json['success'] = 'User Has Been Creared Successfully';

            $json['redirectTo'] = $this->url->link('admin/users');
        }else{
            
            $json['errors'] = $this->validator->flattenMessages();

        }

        return $this->json($json);
    }


    /**
     * Display Edit Form
     * 
     * @param int $id
     * @return string
     */
    public function edit ($id) 
    {
        $user = $this->load->model('User');

        if(! $user->exists($id)){
            return $this->url->redirectTo('/404');
        }

        $user = $user->get($id);

        return $this->form($user);

    }


    /**
     * Display Form
     * 
     * @param \stdClass $user
     * @return string
     */
    public function form($user = null)
    {
        if($user){
            // edit users-group
            $data['target'] = 'edit-user-' . $user->id;

            $data['heading'] = 'Edit ' . $user->first_name . ' ' . $user->last_name;

            $data['action'] = $this->url->link("/admin/users/save/$user->id");

        }else{
            // add users-group
            $data['target'] = 'add-user-form';

            $data['heading'] = 'Add New User';

            $data['action'] = $this->url->link('/admin/users/submit');
            
        }

        $user = (array) $user;

        $data['first_name'] = array_get($user, 'first_name');
        $data['last_name'] = array_get($user, 'last_name');
        $data['status'] = array_get($user, 'status', 'enabled');
        $data['gender'] = array_get($user, 'gender');
        $data['users_group_id'] = array_get($user, 'users_group_id');
        $data['email']    = array_get($user, 'email');
        $data['image']    = array_get($user, 'image');
        $data['birthday'] = array_get($user, 'birthday');

        $data['image'] = '';
        if(! empty($user['image'])){
            $data['image'] = $this->url->link('public/images/' . $user['image']);
        }
        $data['groups'] = $this->load->model('UsersGroup')->all();

        return $this->view->render('admin/users/form', $data);
    }


    /**
     * Submit for creating new User Group
     * 
     * @return string | json
     */
    public function save($id)
    {
        $json = [];
        
        if($this->isValid($id)){

            $this->load->model('User')->update($id);

            $json['success'] = 'User Has Been Updated Successfully';

            $json['redirectTo'] = $this->url->link('admin/users');
        }else{

            $json['errors'] = $this->validator->flattenMessages();
        }

        return $this->json($json);
    }


    /**
     * Validate the form
     * 
     * @param int $id
     * @return bool
     */
    private function isValid($id = null)
    {
        $this->validator->required('first_name', 'First Name Is Required');
        $this->validator->required('last_name', 'Last Name Is Required');
        $this->validator->required('email')->email('email');

        if(is_null($id)){
            $this->validator->required('password')->min('password', 8)->match('password', 'confirm_password');
            $this->validator->requiredFile('image')->image('image');
        }else{
            $this->validator->image('image');
        }

        return $this->validator->passes();
    }


    /**
     * Delete User Group
     * 
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $user = $this->load->model('User');

        if(! $user->exists($id) || $id == 1){
            return $this->url->redirectTo('/404');
        }

        $user->delete($id);
    }
      
}