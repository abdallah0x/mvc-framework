<?php 


namespace App\Controllers\Admin\Common;

use System\Controller;
use System\View\ViewInterface;

class LayoutController extends Controller
{
    /**
     * Render the layout with the given view object
     * 
     * @param \System\View\ViewInterface $view
     * @return mixed
     */
    public function render(ViewInterface $view)
    {
        $data['header']  = $this->load->controller('Admin/Common/Header')->index();
        $data['sidebar'] = $this->load->controller('Admin/Common/Sidebar')->index();
        $data['content'] = $view;
        $data['footer']  = $this->load->controller('Admin/Common/Footer')->index();
    
        return $this->view->render('admin/common/layout', $data);
    }
      
}