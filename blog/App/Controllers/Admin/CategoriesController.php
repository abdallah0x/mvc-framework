<?php 


namespace App\Controllers\Admin;

use System\Controller;

class CategoriesController extends Controller
{
    /**
     * Display Categories List
     * 
     * @return mixed
     */
    public function index()
    {
        $this->html->setTitle('Categories');

        $data['categories'] = $this->load->model('Category')->all();        
        
        $data['success'] = $this->session->has('success') ? $this->session->pull('success') : null;
        
        $view = $this->view->render('admin/categories/list', $data);

        return $this->adminLayout->render($view);
    }


    /**
     * Open Categories Form
     * 
     * @return string
     */
    public function add()
    {
        return $this->form();
    }


    /**
     * Submit for creating new Category
     * 
     * @return string | json
     */
    public function submit()
    {
        $json = [];

        if($this->isValid()){

            $this->load->model('Category')->create();

            $json['success'] = 'Category Has Been Creared Successfully';

            $json['redirectTo'] = $this->url->link('admin/categories');
        }else{
            
            $json['errors'] = $this->validator->flattenMessages();

        }

        return $this->json($json);
    }


    /**
     * Display Edit Form
     * 
     * @param int $id
     * @return string
     */
    public function edit ($id) 
    {
        $category = $this->load->model('Category');

        if(! $category->exists($id)){
            return $this->url->redirectTo('/404');
        }

        $category = $category->get($id);

        // $data['action'] = $this->url->link("/admin/categories/save/$id");
        
        return $this->form($category);

        
        // $data['errors'] = $this->session->has('errors') ? $this->session->pull('errors') : null;

        // $this->html->setTitle('Update ' . $data['category']->name);

        // $view =  $this->view->render('/admin/categories/update', $data);

        // return $this->adminLayout->render($view);
    }


    /**
     * Display Form
     * 
     * @param \stdClass $category 
     * @return string
     */
    public function form($category = null)
    {
        if($category){
            // edit category
            $data['target'] = 'edit-category-' . $category->id;

            $data['heading'] = 'Edit ' . $category->name;

            $data['action'] = $this->url->link("/admin/categories/save/$category->id");

        }else{
            // add category
            $data['target'] = 'add-category-form';

            $data['heading'] = 'Add New Category';

            $data['action'] = $this->url->link('/admin/categories/submit');
            
        }

        $data['name'] = $category ? $category->name : null;
        $data['status'] = $category ? $category->status : 'enabled';

        return $this->view->render('admin/categories/form', $data);
    }


    /**
     * Submit for creating new Category
     * 
     * @return string | json
     */
    public function save($id)
    {
        $json = [];
        
        if($this->isValid()){

            $this->load->model('Category')->update($id);

            $json['success'] = 'Category Has Been Updated Successfully';

            $json['redirectTo'] = $this->url->link('admin/categories');
        }else{

            $json['errors'] = $this->validator->flattenMessages();
        }

        return $this->json($json);
    }


    /**
     * Validate the form
     * 
     * @return bool
     */
    private function isValid()
    {
        $this->validator->required('name', 'Category Name Is Required');

        return $this->validator->passes();
    }


    /**
     * Delete Category
     * 
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $category = $this->load->model('Category');

        if(! $category->exists($id)){
            return $this->url->redirectTo('/404');
        }

        $category->delete($id);
    }
      
}