<?php include 'include/header.php';

$id = $_GET['id'];

   //create object
    $db = new database();
    //create query
    $query = "SELECT * FROM posts WHERE id = '$id'";
    //run query
    $post = $db->select($query)->fetch_assoc();
    
    
    //create query
    $query = "SELECT * FROM categories";
    //run query
    $categories = $db->select($query);
?>

<?php
    if(isset($_POST['submit'])){
		//Assign Vars
		$title    = mysqli_real_escape_string($db->link, $_POST['title']);
		$body     = mysqli_real_escape_string($db->link, $_POST['body']);
		$category = mysqli_real_escape_string($db->link, $_POST['category']);
		$author   = mysqli_real_escape_string($db->link, $_POST['auther']);
		$tags     = mysqli_real_escape_string($db->link, $_POST['tags']);
		//Simple Validation
		if($title == '' || $body == '' || $category == '' || $author == ''){
			//Set Error
			$error = 'Please fill out all required fields';
		} else {
			$query = "UPDATE posts SET 
                                    title    = '$title',
                                    body     = '$body',
                                    category = '$category',
                                    auther   = '$author',
                                    tags     = '$tags' WHERE id = '$id'";
			
			$update_row = $db->update($query);
		}
	}


?>
<?php 
    if(isset($_POST['delete'])){
        $query = "DELETE FROM posts WHERE id = '$id'";
        $delete = $db->delete($query);
    }

?>
<form method="post" action="edit_post.php?id=<?php echo $id; ?>">
  <div class="form-group">
    <label>Post Title</label>
    <input name="title" type="text" class="form-control" placeholder="Enter Title" value="<?php echo $post['title'];?>">
  </div>
    <div class="form-group">
    <label>Post Body</label>
    <textarea name="body" class="form-control" placeholder="Enter Post Body"><?php echo $post['body'];?></textarea>
  </div>
    <div class="form-group">
    <label>Category</label>
    <select name="category" class="form-control">
        <?php while ($row = $categories->fetch_assoc()){ ?>
        <?php if($row['id'] == $post['category']) { $selected = 'selected';}
            else {$selected = '';}?>
        <option <?php echo $selected; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php }?>
    </select>
  </div>
    <div class="form-group">
    <label>Post Auther</label>
    <input name="auther" type="text" class="form-control" placeholder="Enter auther" value="<?php echo $post['auther'];?>">
  </div>
     <div class="form-group">
    <label>Post Tags</label>
    <input name="tags" type="text" class="form-control" placeholder="Enter tags" value ="<?php echo $post['tags'];?>">
  </div>
  
    <input name="submit" value="submit" type="submit" class="btn btn-default">
    <a href="index.php" class="btn btn-default">Cancel</a>
    <input name="delete" value="delete" type="submit" class="btn btn-danger">
</form>


<?php include 'include/footer.php'; ?>