
<?php include 'include/header.php'; ?>

<?php

    $db = new database();
    $query = "SELECT posts.* , categories.name FROM posts INNER JOIN categories ON posts.category = categories.id";
    $posts = $db->select($query);
?>
    <table class="table table-striped table-bordered">
        <tr>
            <th>id</th>
            <th>Post Title</th>
            <th>category</th>
            <th>auther</th>
            <th>date</th>
        </tr>
        <?php while ($row = $posts->fetch_assoc()){ ?>
        <tr>           
             <td><?php echo $row['id'];?></td>
             <td><a href="edit_post.php?id=<?php echo $row['id'];?>"><?php echo $row['title'];?></a></td>
             <td><?php echo $row['name'];?></td>
             <td><?php echo $row['auther'];?></td>
             <td><?php echo formatDate($row['date']);?></td>
        </tr>
         <?php }?>
    </table>

<?php

    $db = new database();
    $query = "SELECT * FROM categories";
    $category = $db->select($query);
?>

    <table class="table table-striped table-bordered">
        <tr>
            <th>id</th>
            <th>Category Name</th>
        </tr>
        <?php while ($row = $category->fetch_assoc()){ ?>
        <tr>
             <td><?php echo $row['id'];?></td>
             <td><a href="edit_category.php?id=<?php echo $row['id'];?>"><?php echo $row['name'];?></a></td>
        </tr>
        <?php }?>
    </table>


<?php include 'include/footer.php'; ?>