<?php 

/**
 * Description of Loader Class
 * 
 * TThis class is responsible for get call classes from Application class to controllers classes 
 *
 * @author abdallah
 */

namespace System;

abstract class Controller
{

	/**
	 * Application Object
	 *
	 * @var \System\Application
	 */
	protected $app;


	/**
	 * Errors Container
	 * 
	 * @var array
	 */
	protected $errors;


	/**
	 * Constructor
	 *
	 * @param \System\Application $app
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;
	}


	/**
	 * Encode the given value to json
	 * 
	 * @param mixed $data
	 * 
	 * @return string
	 */
	public function json($data)
	{
		return json_encode($data);
	}


	/**
	 * Call shared application objects dynamically
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function __get($key)
	{
		return $this->app->get($key);
	}
}