<?php 

namespace System;

class Validation
{

	/**
	 * Application Object
	 *
	 * @var \System\Application
	 */
	private $app;


	/**
	 * Errors Container
	 * 
	 * @var array
	 */
	private $errors = [];


	/**
	 * Constructor
	 *
	 * @param \System\Application $app
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;
	}


	/**
	 * Determine if the given input is not empty
	 * 
	 * @param string $inputName
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function required($inputName, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);

		if($inputValue === ''){
			$message = $customErrorMessage ?: sprintf('%s Is Required', $inputName);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input is valid email
	 * 
	 * @param string $inputName
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function email($inputName, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);

		if(! filter_var($inputValue, FILTER_VALIDATE_EMAIL)){
			$message = $customErrorMessage ?: sprintf('%s Is not valid email', $inputName);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input has float value
	 * 
	 * @param string $inputName
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function float($inputName, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);

		if(is_float($inputValue)){
			$message = $customErrorMessage ?: sprintf('%s Accept Only Float', $inputName);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input value should be at least the given length
	 * 
	 * @param string $inputName
	 * @param int $length
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function min($inputName, $length, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);

		if(strlen($inputValue) < $length){
			$message = $customErrorMessage ?: sprintf('%s should be at least %d', $inputName, $length);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input value should be at most the given length
	 * 
	 * @param string $inputName
	 * @param int $length
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function max($inputName, $length, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);

		if(strlen($inputValue) > $length){
			$message = $customErrorMessage ?: sprintf('%s should be at most %d', $inputName, $length);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input matches the second unput
	 * 
	 * @param string $firstName
	 * @param string $secondName
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function match($firstName, $secondName, $customErrorMessage = null)
	{
		if($this->hasErrors($firstName)){
			return $this;
		}

		$first = $this->value($firstName);
		$second = $this->value($secondName);

		if($first != $second){
			$message = $customErrorMessage ?: sprintf('%s should match %s', $secondName, $firstName);
			
			$this->addError($first, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input unique in database
	 * 
	 * @param string $inputName
	 * @param array $databaseData
	 * @param string $customErrorMessage
	 * 
	 * @return $this
	 */
	public function unique($inputName, array $databaseData, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$inputValue = $this->value($inputName);
		
		$table  = null;
		$column = null;
		$exceptionColumn = null;
		$exceptionColumnValue = null;

		if(count($databaseData) == 2){
			list($table, $column) = $databaseData;
		}elseif(count($databaseData) == 4){
			list($table, $column, $exceptionColumn, $exceptionColumnValue) = $databaseData;
		}

		if($exceptionColumn && $exceptionColumnValue){
			$result = $this->db->select($column)
								->from($table)
								->where($column . '= ? AND ' . $exceptionColumn . ' != ?', $inputValue, $exceptionColumnValue)
								->fetch();
		}else{
			$result = $this->db->select($column)
								->from($table)
								->where($column . '=?', $inputValue)
								->fetch();
		}

			
			if($result){
				$message = $customErrorMessage ?: sprintf('%s already exist', $inputName, $length);
				$this->addError($inputName, $message);
			}

		return $this;
	}


	/**
	 * Determine if the given input file exists
	 * 
	 * @param string $inputName
	 * @param string $customErrorMessage
	 * @return $this
	 */
	public function requiredFile($inputName, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$file = $this->app->request->file($inputName);

		if(! $file->exists()){
			$message = $customErrorMessage ?: sprintf('%s Is Required', $inputName);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Determine if the given input file Image
	 * 
	 * @param string $inputName
	 * @param string $customErrorMessage
	 * @return $this
	 */
	public function image($inputName, $customErrorMessage = null)
	{
		if($this->hasErrors($inputName)){
			return $this;
		}

		$file = $this->app->request->file($inputName);

		if(! $file->exists()){
			return $this;
		}

		if(! $file->isImage()){
			$message = $customErrorMessage ?: sprintf('%s Is not valid Image', $inputName);
		
			$this->addError($inputName, $message);
		}

		return $this;
	}


	/**
	 * Add Custom Message
	 * 
	 * @param string $message
	 * 
	 * @return $this
	 */
	public function message($message)
	{
		$this->errors[] = $message;

		return $this;
	}


	/**
	 * Validate All Inputs
	 * 
	 * @return $this
	 */
	public function validate()
	{
		
	}


	
	/**
	 * Determine if there are any invalid inputs
	 * 
	 * @return bool
	 */
	public function fails()
	{
		return ! empty($this->errors);
	}


	/**
	 * Determine if all inputs are valid
	 * 
	 * @return bool
	 */
	public function passes()
	{
		return empty($this->errors);
		
	}


	/**
	 * Get all errors
	 * 
	 * @return array
	 */
	public function getMessages()
	{
		return $this->errors;
	}


	/**
	 * Flatten errors and make it as a string imploded with break
	 * 
	 * @return string
	 */
	public function flattenMessages()
	{
		return implode('<br>', $this->errors);
	}


	/**
	 * Get the value for the given input
	 * 
	 * @param string $input
	 * 
	 * @return bool
	 */
	private function value($input)
	{
		return $this->app->request->post($input);
	}


	/**
	 * Add input Error
	 * 
	 * @param string $inputName
	 * @param string $errorMessage
	 * 
	 * @return void
	 */
	private function addError($inputName, $errorMessage)
	{
		$this->errors[$inputName] = $errorMessage;
	}


	/**
	 * Determine if the given input has previous errors
	 * 
	 * @param string $inputName
	 * 
	 * @return bool
	 */
	private function hasErrors($inputName)
	{
		return array_key_exists($inputName, $this->errors);
	}
	
}